class Car {

    constructor(y) {
        this.engine = y ? y :'mt-348';
        this.steeringWheel = 'sport';
    }

    start() {
        console.log('startted');
    }
    stop() {
        console.log('stopped');
    }

    openDoors(side) {
        console.log(`opened ${side}`);
    }
}

class BMW {
    constructor () {
        this.model = 'i8';
    }

    openDoors(side) {
        console.log(`opened BMW ${side}`);
    }
} 

class Audi extends Car {
    constructor (model = 'A8', x) {
        super ();
        this.model = model;
    }
}

let audi = new Audi('A7', 8888);

console.log(audi.model);

class Tesla extends Car {
    constructor (model = 'model3') {
        super ();
        this.model = model;
    }
}

let tesla = new Tesla('model3');

console.log(tesla.model);

/* audi.openDoors('left');

let bmw = new BMW();

bmw.openDoors('right'); */